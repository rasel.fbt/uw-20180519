<?php

namespace App;

use App\Models\Company;
use App\Models\OauthAccessToken;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'first_name', 'last_name', 'phone', 'email', 'password', 'gender', 'type', 'has_accepted_policy'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'has_accepted_policy'
    ];

    /**
     * Set the user's first name.
     *
     * @param  string $value
     * @return void
     */
    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = strtolower($value);

        $this->generateNameAttribute();
    }

    /**
     * Generate and set user's name
     *
     * @return void
     */
    private function generateNameAttribute()
    {
        $name = '';
        $name .= isset($this->attributes['first_name']) ? $this->attributes['first_name'] : '';
        $name .= isset($this->attributes['last_name']) ? ' ' . $this->attributes['last_name'] : '';

        $this->attributes['name'] = $name;
    }

    /**
     * Set the user's last name.
     *
     * @param  string $value
     * @return void
     */
    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = strtolower($value);

        $this->generateNameAttribute();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function AauthAcessToken()
    {
        return $this->hasMany(OauthAccessToken::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function phone()
    {
        return $this->hasOne(Company::class);
    }
}
