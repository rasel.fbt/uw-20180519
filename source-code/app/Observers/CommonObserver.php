<?php

namespace App\Observers;

use Illuminate\Support\Facades\Auth;

class CommonObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  $model
     * @return void
     */
    public function creating($model)
    {
        if (Auth::check()) {
            $model->created_by = Auth::user()->id;
        }
    }

    /**
     * Listen to the User created event.
     *
     * @param  $model
     * @return void
     */
    public function updating($model)
    {
        if (Auth::check()) {
            $model->updated_by = Auth::user()->id;
        }
    }

    /**
     * Listen to the User created event.
     *
     * @param  $model
     * @return void
     */
    public function deleting($model)
    {
        if (Auth::check()) {
            $model->deleted_by = Auth::user()->id;
            $model->save();
        }
    }
}
