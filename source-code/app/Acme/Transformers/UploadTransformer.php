<?php
/**
 * Created by PhpStorm.
 * User: Rasel
 * Date: 06/21/2018
 * Time: 6:58 PM
 */

namespace App\Acme\Transformers;

class UploadTransformer extends Transformer
{
    /**
     * @param array $item
     * @return array
     */
    public function transform(array $item)
    {
        $transformedItem = [
            'id' => $item['id'],
            'path' => $item['path'],
            'full_path' => asset('storage/'. $item['path']),
            'orig_filename' => $item['orig_filename'],
            'extension' => $item['extension'],
            'title' => $item['title'],
            'description' => $item['description'],
        ];

        return $transformedItem;
    }
}
