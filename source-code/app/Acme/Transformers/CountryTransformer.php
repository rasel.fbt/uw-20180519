<?php
/**
 * Created by PhpStorm.
 * User: Rasel
 * Date: 06/21/2018
 * Time: 6:58 PM
 */

namespace App\Acme\Transformers;


class CountryTransformer extends Transformer
{
    /**
     * @var Transformer Variables
     */
    protected $districtTransformer;

    /**
     * ConsignmentTransformer constructor.
     *
     * @param DistrictTransformer $districtTransformer
     */
    public function __construct(
        DistrictTransformer $districtTransformer
    )
    {
        $this->districtTransformer = $districtTransformer;
    }

    /**
     * @param array $item
     * @return array
     */
    public function transform(array $item)
    {
        $transformedItem = [
            'id' => $item['id'],
            'name' => $item['name'],
            'districts' => []
        ];

        if (isset($item['districts'])) {
            $transformedItem['districts'] = $this->districtTransformer->transformCollection($item['districts']);
        }

        return $transformedItem;
    }
}
