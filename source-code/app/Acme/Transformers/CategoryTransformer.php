<?php
/**
 * Created by PhpStorm.
 * User: Rasel
 * Date: 06/21/2018
 * Time: 6:58 PM
 */

namespace App\Acme\Transformers;


use App\Http\Traits\FileTrait;

class CategoryTransformer extends Transformer
{
    use FileTrait;

    /**
     * @var Transformer Variables
     */
    protected $keywordTransformer;

    /**
     * ConsignmentTransformer constructor.
     *
     * @param KeywordTransformer $keywordTransformer
     */
    public function __construct(
        KeywordTransformer $keywordTransformer
    )
    {
        $this->keywordTransformer = $keywordTransformer;
    }

    /**
     * @param array $item
     * @return array
     */
    public function transform(array $item)
    {
        $transformedItem = [
            'id' => $item['id'],
            'name' => $item['name'],
            'parent_id' => $item['parent_id'],
            'order' => $item['order'],
            'type' => is_null($item['parent_id']) ? 'category' : 'subcategory',
            'category' => null,
            'subcategories' => [],
            'keywords' => []
        ];

        if (isset($item['category'])) {
            $transformedItem['category'] = $this->transform($item['category']);
        }

        if (isset($item['subcategories'])) {
            $transformedItem['subcategories'] = $this->transformCollection($item['subcategories']);
        }

        if (isset($item['keywords'])) {
            $transformedItem['keywords'] = $this->keywordTransformer->transformCollection($item['keywords']);
        }

        if (isset($item['image'])) {
            $transformedItem['images'] = $this->getFiles($item['image']);
        }

        return $transformedItem;
    }
}
