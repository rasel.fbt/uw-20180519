<?php
/**
 * Created by PhpStorm.
 * User: Rasel
 * Date: 06/21/2018
 * Time: 6:58 PM
 */

namespace App\Acme\Transformers;

class JobPostTransformer extends Transformer
{
    /**
     * @var Transformer Variables
     */
    protected $categoryTransformer,
        $subcategoryTransformer,
        $fieldTransformer,
        $skillTransformer;

    /**
     * ConsignmentTransformer constructor.
     *
     * @param CategoryTransformer $categoryTransformer
     * @param CategoryTransformer $subcategoryTransformer
     * @param FieldTransformer $fieldTransformer
     * @param SkillTransformer $skillTransformer
     */
    public function __construct(
        CategoryTransformer $categoryTransformer,
        CategoryTransformer $subcategoryTransformer,
        FieldTransformer $fieldTransformer,
        SkillTransformer $skillTransformer
    )
    {
        $this->categoryTransformer = $categoryTransformer;
        $this->subcategoryTransformer = $subcategoryTransformer;
        $this->fieldTransformer = $fieldTransformer;
        $this->skillTransformer = $skillTransformer;
    }

    /**
     * @param array $item
     * @return array
     */
    public function transform(array $item)
    {
        $transformedItem = [
            'user_id' => $item['user_id'] ?? null,
            'title' => $item['title'] ?? null,
            'description' => $item['description'] ?? null,
            'files' => $item['files'] ?? null,
            'inquiry_type' => $item['inquiry_type'] ?? null,
            'offer_details' => $item['offer_details'] ?? null,
            'contract_type' => $item['contract_type'] ?? null,
            'work_start_type' => $item['work_start_type'] ?? null,
            'work_start_date' => $item['work_start_date'] ?? null,
            'work_delivery_type' => $item['work_delivery_type'] ?? null,
            'work_delivery_date' => $item['work_delivery_date'] ?? null,
            'payment_type' => $item['payment_type'] ?? null,
            'payment_hours' => $item['payment_hours'] ?? null,
            'payment_budget' => $item['payment_budget'] ?? null,
            'payout_type' => $item['payout_type'] ?? null,
            'status' => $item['status'] ?? null,
            'approved_by' => $item['approved_by'] ?? null,
            'categories' => null,
            'subcategories' => null,
            'fields' => null,
            'skills' => null,
        ];

        if (isset($item['categories'])) {
            $transformedItem['categories'] = $this->categoryTransformer->transformCollection($item['categories']);
        }

        if (isset($item['subcategories'])) {
            $transformedItem['subcategories'] = $this->subcategoryTransformer->transformCollection($item['subcategories']);
        }

        if (isset($item['fields'])) {
            $transformedItem['fields'] = $this->fieldTransformer->transformCollection($item['fields']);
        }

        if (isset($item['skills'])) {
            $transformedItem['skills'] = $this->skillTransformer->transformCollection($item['skills']);
        }

        return $transformedItem;
    }
}
