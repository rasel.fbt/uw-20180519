<?php namespace App\Acme\Transformers;
/**
 * Created by PhpStorm.
 * User: Rasel
 * Date: 06/21/2018
 * Time: 6:58 PM
 */

abstract class Transformer
{
    public function transformCollection(array $items)
    {
        return array_map([$this, 'transform'], $items);
    }

    public abstract function transform(array $item);
}