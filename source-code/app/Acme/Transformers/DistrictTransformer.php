<?php
/**
 * Created by PhpStorm.
 * User: Rasel
 * Date: 06/21/2018
 * Time: 6:58 PM
 */

namespace App\Acme\Transformers;


class DistrictTransformer extends Transformer
{
    /**
     * @var Transformer Variables
     */
    protected $cityTransformer;

    /**
     * ConsignmentTransformer constructor.
     *
     * @param CityTransformer $cityTransformer
     */
    public function __construct(
        CityTransformer $cityTransformer
    )
    {
        $this->cityTransformer = $cityTransformer;
    }

    /**
     * @param array $item
     * @return array
     */
    public function transform(array $item)
    {
        $transformedItem = [
            'id' => $item['id'],
            'name' => $item['name'],
            'cities' => [],
        ];

        if (isset($item['cities'])) {
            $transformedItem['cities'] = $this->cityTransformer->transformCollection($item['cities']);
        }

        return $transformedItem;
    }
}
