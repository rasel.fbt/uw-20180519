<?php
/**
 * Created by PhpStorm.
 * User: Rasel
 * Date: 06/21/2018
 * Time: 6:58 PM
 */

namespace App\Acme\Transformers;


class CompanyTransformer extends Transformer
{
    /**
     * @var Transformer Variables
     */
    protected $companyTypeTransformer,
        $categoryTransformer,
        $countryTransformer,
        $districtTransformer,
        $cityTransformer,
        $languageTransformer;

    /**
     * ConsignmentTransformer constructor.
     *
     * @param CompanyTypeTransformer $companyTypeTransformer
     * @param CategoryTransformer $categoryTransformer
     * @param CountryTransformer $countryTransformer
     * @param DistrictTransformer $districtTransformer
     * @param CityTransformer $cityTransformer
     * @param LanguageTransformer $languageTransformer
     */
    public function __construct(
        CompanyTypeTransformer $companyTypeTransformer,
        CategoryTransformer $categoryTransformer,
        CountryTransformer $countryTransformer,
        DistrictTransformer $districtTransformer,
        CityTransformer $cityTransformer,
        LanguageTransformer $languageTransformer
    )
    {
        $this->companyTypeTransformer = $companyTypeTransformer;
        $this->categoryTransformer = $categoryTransformer;
        $this->countryTransformer = $countryTransformer;
        $this->districtTransformer = $districtTransformer;
        $this->cityTransformer = $cityTransformer;
        $this->languageTransformer = $languageTransformer;
    }

    /**
     * @param array $item
     * @return array
     */
    public function transform(array $item)
    {
        $transformedItem = [
            'id' => $item['id'],
            'name' => $item['name'],
            'type' => null,
            'category' => null,
            'subcategory' => null,
            'country' => null,
            'district' => null,
            'city' => null,
            'fax' => $item['fax'],
            'hrb_number' => $item['hrb_number'],
            'st_number' => $item['st_number'],
            'umst_id' => $item['umst_id'],
            'language' => null,
            'about_us' => $item['about_us'],
            'job_complete' => $item['name'],
            'status' => $item['status']
        ];

        if (isset($item['type'])) {
            $transformedItem['type'] = $this->companyTypeTransformer->transform($item['type']);
        }

        if (isset($item['category'])) {
            $transformedItem['category'] = $this->categoryTransformer->transform($item['category']);
        }

        if (isset($item['subcategory'])) {
            $transformedItem['subcategory'] = $this->categoryTransformer->transform($item['subcategory']);
        }

        if (isset($item['country'])) {
            $transformedItem['country'] = $this->countryTransformer->transform($item['country']);
        }

        if (isset($item['district'])) {
            $transformedItem['district'] = $this->districtTransformer->transform($item['district']);
        }

        if (isset($item['city'])) {
            $transformedItem['city'] = $this->cityTransformer->transform($item['city']);
        }

        if (isset($item['language'])) {
            $transformedItem['language'] = $this->languageTransformer->transform($item['language']);
        }

        return $transformedItem;
    }
}
