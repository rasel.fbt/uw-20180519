<?php

namespace App\Models;

use App\Http\Traits\CommonModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Keyword extends Model
{
    use SoftDeletes, CommonModelTrait;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'name', 'created_by', 'updated_by', 'deleted_by'];

    /**
     * Get the category that owns the keyword.
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
