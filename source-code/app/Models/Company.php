<?php

namespace App\Models;

use App\Http\Traits\CommonModelTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes, CommonModelTrait;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'type_id',
        'category_id',
        'subcategory_id',
        'country_id',
        'district_id',
        'city_id',
        'address',
        'fax',
        'hrb_number',
        'st_number',
        'umst_id',
        'language_id',
        'about_us',
        'job_complete',
        'status',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Get the company type that owns the company.
     */
    public function type()
    {
        return $this->belongsTo(CompanyType::class);
    }

    /**
     * Get the category that owns the company.
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get the subcategory that owns the company.
     */
    public function subcategory()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get the country that owns the company.
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Get the district that owns the company.
     */
    public function district()
    {
        return $this->belongsTo(District::class);
    }

    /**
     * Get the city that owns the company.
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Get the language that owns the company.
     */
    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
