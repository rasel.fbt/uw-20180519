<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobPost extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'title', 'description', 'files', 'inquiry_type', 'offer_details', 'contract_type',
        'work_start_type', 'work_start_date', 'work_delivery_type', 'work_delivery_date', 'payment_type', 'payment_hours',
        'payment_budget', 'payout_type', 'status', 'approved_by'];

    /**
     * The categories that belong to the job post.
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'job_post_category', 'job_post_id', 'category_id');
    }

    /**
     * The subcategories that belong to the job post.
     */
    public function subcategories()
    {
        return $this->belongsToMany(Category::class, 'job_post_subcategory', 'job_post_id', 'subcategory_id');
    }

    /**
     * The fields that belong to the job post.
     */
    public function fields()
    {
        return $this->belongsToMany(Field::class, 'job_post_field');
    }

    /**
     * The skills that belong to the job post.
     */
    public function skills()
    {
        return $this->belongsToMany(Skill::class, 'job_post_skill');
    }
}
