<?php

namespace App\Models;

use App\Http\Traits\CommonModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes, CommonModelTrait;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'district_id', 'created_by', 'updated_by', 'deleted_by'];

    /**
     * Get the district that owns the city.
     */
    public function district()
    {
        return $this->belongsTo(City::class);
    }
}
