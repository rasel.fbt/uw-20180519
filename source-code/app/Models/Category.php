<?php

namespace App\Models;

use App\Http\Traits\CommonModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes, CommonModelTrait;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'image', 'parent_id', 'order', 'created_by', 'updated_by', 'deleted_by'];

    /**
     * Get the sub categories for the category.
     */
    public function subcategories()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    /**
     * Get the category that owns the sub category.
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    /**
     * Get the keywords for the category.
     */
    public function keywords()
    {
        return $this->hasMany(Keyword::class);
    }
}
