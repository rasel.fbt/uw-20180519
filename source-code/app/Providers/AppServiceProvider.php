<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\City;
use App\Models\Company;
use App\Models\CompanyType;
use App\Models\Country;
use App\Models\District;
use App\Models\Keyword;
use App\Models\Language;
use App\Models\PolicyTerm;
use App\Observers\CommonObserver;
use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        /* Model Observer */
        User::observe(CommonObserver::class);
        CompanyType::observe(CommonObserver::class);
        Category::observe(CommonObserver::class);
        Keyword::observe(CommonObserver::class);
        Country::observe(CommonObserver::class);
        District::observe(CommonObserver::class);
        City::observe(CommonObserver::class);
        Language::observe(CommonObserver::class);
        Company::observe(CommonObserver::class);
        PolicyTerm::observe(CommonObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
