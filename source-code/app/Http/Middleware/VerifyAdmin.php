<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->type !== 'admin') {
            return response()->json([
                'errors' => [
                    'message' => 'You are not authorized',
                ]
            ], 401);
        }

        return $next($request);
    }
}
