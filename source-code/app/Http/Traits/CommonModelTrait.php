<?php
/**
 * Created by PhpStorm.
 * User: Rasel
 * Date: 06/21/2018
 * Time: 6:58 PM
 */

namespace App\Http\Traits;

use App\User;

trait CommonModelTrait
{
    /**
     * Get the user who created the model.
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * Get the user who updated the model.
     */
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    /**
     * Get the user who deleted the model.
     */
    public function deletedBy()
    {
        return $this->belongsTo(User::class, 'deleted_by');
    }
}
