<?php
/**
 * Created by PhpStorm.
 * User: Sovon
 * Date: 7/21/2018
 * Time: 10:33 AM
 */

namespace App\Http\Traits;

use App\Models\Upload;
use App\Acme\Transformers\UploadTransformer;

trait FileTrait
{
    /**
     * Format and save files
     *
     * @param array $files
     * @return array|string
     */
    public function saveFiles(array $files)
    {
        $files = json_encode($files);

        return $files;
    }

    /**
     * Get files array
     *
     * @param $fileJson
     * @return array
     */
    public function getFiles($fileJson)
    {
        $fileIds = json_decode($fileJson);

        $files = Upload::whereIn('id', $fileIds)
            ->get()
            ->toArray();

        $uploadTransformer = new UploadTransformer();

        return $uploadTransformer->transformCollection($files);
    }
}
