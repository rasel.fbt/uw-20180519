<?php

namespace App\Http\Controllers\Api;

use App\Acme\Transformers\UploadTransformer;
use App\Models\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UploadApiController extends ApiController
{
    public $successStatus = 200;
    /**
     * @var uploadTransformer
     */
    protected $uploadTransformer;

    /**
     * AuthApiController constructor.
     *
     * @param UploadTransformer $uploadTransformer
     */
    public function __construct(UploadTransformer $uploadTransformer)
    {
        $this->uploadTransformer = $uploadTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|file'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $file = $request->file('file');
        $originalName = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        $fileName = Storage::disk('public')->putFile('uploads', $file);

        $upload = Upload::create([
            'path' => $fileName,
            'orig_filename' => $originalName,
            'extension' => $extension,
            'title' => $request->input('title'),
            'description' => $request->input('description')
        ]);

        return response()->json([
            'success' => [
                'message' => 'Success',
                'file' => $this->uploadTransformer->transform($upload->toArray())
            ]
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $upload = Upload::find($id);

        if (is_null($upload)) {
            return $this->respondNotFound('File not found');
        }

        try {
            Storage::disk('public')->delete($upload->path);
        } catch (\Exception $e) {
            // File not found
        }

        $upload->delete();

        return response()->json([
            'success' => [
                'message' => 'Successfully deleted',
                'file' => $this->uploadTransformer->transform($upload->toArray())
            ]
        ]);
    }
}
