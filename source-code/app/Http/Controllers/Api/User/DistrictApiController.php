<?php

namespace App\Http\Controllers\Api\User;

use App\Acme\Transformers\DistrictTransformer;
use App\Models\District;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\ApiController;

class DistrictApiController extends ApiController
{
    public $successStatus = 200;
    /**
     * @var DistrictTransformer
     */
    protected $districtTransformer;

    /**
     * AuthApiController constructor.
     *
     * @param DistrictTransformer $districtTransformer
     */
    public function __construct(DistrictTransformer $districtTransformer)
    {
        $this->districtTransformer = $districtTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginateData = District::with('country')->paginate(10)
            ->toArray();

        $districts = $paginateData['data'];

        unset($paginateData['data']);

        return response()->json([
            'success' => [
                'message' => 'Success',
                'districts' => $this->districtTransformer->transformCollection($districts),
                'pagination' => $paginateData
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'country_id' => 'required|exists:countries,id'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $inputs = $request->all();

        $district = District::create($inputs);

        return $this->show($district->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $district = District::with('country')
            ->where('id', $id)
            ->first();

        if (is_null($district)) {
            return $this->respondNotFound('District not found');
        }

        return response()->json([
            'success' => [
                'message' => 'Success',
                'district' => $this->districtTransformer->transform($district->toArray())
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'country_id' => 'required|exists:countries,id'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $district = District::find($id);

        if (is_null($district)) {
            return $this->respondNotFound('District not found');
        }

        $inputs = $request->all();

        $district->update($inputs);

        return $this->show($district->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $district = District::find($id);

        if (is_null($district)) {
            return $this->respondNotFound('District not found');
        }

        try {
            $district->delete();
        } catch (\Exception $e) {
            Log::error('District delete failed @DistrictApiController@destroy');
        }

        return response()->json([
            'success' => [
                'message' => 'Successfully deleted district',
                'district' => $district
            ]
        ], $this->successStatus);
    }
}
