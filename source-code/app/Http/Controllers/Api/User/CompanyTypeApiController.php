<?php

namespace App\Http\Controllers\Api\User;

use App\Acme\Transformers\CompanyTypeTransformer;
use App\Http\Controllers\Api\ApiController;
use App\Models\CompanyType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CompanyTypeApiController extends ApiController
{
    public $successStatus = 200;

    /**
     * @var CompanyTypeTransformer
     */
    protected $companyTypeTransformer;

    /**
     * AuthApiController constructor.
     *
     * @param CompanyTypeTransformer $companyTypeTransformer
     */
    public function __construct(CompanyTypeTransformer $companyTypeTransformer)
    {
        $this->companyTypeTransformer = $companyTypeTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginateData = CompanyType::paginate(10)
            ->toArray();

        $companyTypes = $paginateData['data'];

        unset($paginateData['data']);

        return response()->json([
            'success' => [
                'message' => 'Success',
                'companyTypes' => $this->companyTypeTransformer->transformCollection($companyTypes),
                'pagination' => $paginateData
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $inputs = $request->all();

        $companyType = CompanyType::create($inputs);

        return $this->show($companyType->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $companyType = CompanyType::find($id);

        if (is_null($companyType)) {
            return $this->respondNotFound('Company type not found');
        }

        return response()->json([
            'success' => [
                'message' => 'Success',
                'companyType' => $this->companyTypeTransformer->transform($companyType->toArray())
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $companyType = CompanyType::find($id);

        if (is_null($companyType)) {
            return $this->respondNotFound('Company type not found');
        }

        $inputs = $request->all();

        $companyType->update($inputs);

        return $this->show($companyType->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $companyType = CompanyType::find($id);

        if (is_null($companyType)) {
            return $this->respondNotFound('Company type not found');
        }

        try {
            $companyType->delete();
        } catch (\Exception $e) {
            Log::error('CompanyType delete failed @CompanyTypeApiController@destroy');
        }

        return response()->json([
            'success' => [
                'message' => 'Successfully deleted company type',
                'companyType' => $companyType
            ]
        ], $this->successStatus);
    }
}
