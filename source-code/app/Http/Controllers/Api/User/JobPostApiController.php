<?php

namespace App\Http\Controllers\Api\User;

use App\Models\Field;
use App\Models\Skill;
use App\Models\JobPost;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Acme\Transformers\JobPostTransformer;
use App\Http\Controllers\Api\ApiController;

class JobPostApiController extends ApiController
{
    public $successStatus = 200;

    /**
     * @var JobPostTransformer
     */
    private $jobPostTransformer;

    /**
     * JobPostApiController constructor.
     *
     * @param JobPostTransformer $jobPostTransformer
     */
    public function __construct(JobPostTransformer $jobPostTransformer)
    {
        $this->jobPostTransformer = $jobPostTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $status = $request->input('status');

        $jobPosts = new JobPost();

        if ($status != 'all') {
            $jobPosts->where('status', $status);
        }

        $paginateData = $jobPosts->paginate(10)
            ->toArray();

        $jobPosts = $paginateData['data'];

        unset($paginateData['data']);

        return response()->json([
            'success' => [
                'message' => 'Success',
                'cities' => $this->jobPostTransformer->transformCollection($jobPosts),
                'pagination' => $paginateData
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
//            'files' => 'required',
            'inquiry_type' => 'required',
//            'offer_details' => 'required',
            'contract_type' => 'required',
            'work_start_type' => 'required',
//            'work_start_date' => 'required',
            'work_delivery_type' => 'required',
//            'work_delivery_date' => 'required',
            'payment_type' => 'required',
//            'payment_hours' => 'required',
//            'payment_budget' => 'required',
            'payout_type' => 'required',
            'categories' => 'required',
            'subcategories' => 'required',
            'fields' => 'required',
            'skills' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $inputs = $request->all();
        $inputs['user_id'] = Auth::user()->id;
        $inputs['payment_hours'] = $request->input('payment_hours') ?? 0;
        $inputs['payment_budget'] = $request->input('payment_budget') ?? 0;

        $jobPost = JobPost::create($inputs);

        // Add categories
        if ($request->has('categories')) {
            $categories = explode(',', $request->input('categories'));
            $categories = Category::whereIn('id', $categories)
                ->get()->pluck('id');

            $jobPost->categories()->sync($categories);
        }

        // Add subcategories
        if ($request->has('subcategories')) {
            $subcategories = explode(',', $request->input('subcategories'));
            $subcategories = Category::whereIn('id', $subcategories)
                ->get()->pluck('id');

            $jobPost->subcategories()->sync($subcategories);
        }

        // Add fields
        if ($request->has('fields')) {
            $fields = explode(',', $request->input('fields'));
            $fields = Field::whereIn('id', $fields)
                ->get()->pluck('id');

            $jobPost->fields()->sync($fields);
        }

        // Add skills
        if ($request->has('skills')) {
            $skills = explode(',', $request->input('skills'));
            $skills = Skill::whereIn('id', $skills)
                ->get()->pluck('id');

            $jobPost->skills()->sync($skills);
        }

        return $this->show($jobPost->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $jobPost = JobPost::with(['categories', 'subcategories', 'fields', 'skills'])
            ->where('id', $id)
            ->first();

        if (is_null($jobPost)) {
            return $this->respondNotFound('Job post not found');
        }

        return response()->json([
            'success' => [
                'message' => 'Success',
                'job' => $this->jobPostTransformer->transform($jobPost->toArray())
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
