<?php

namespace App\Http\Controllers\Api\User;

use App\Acme\Transformers\CompanyTransformer;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\ApiController;

class CompanyApiController extends ApiController
{
    public $successStatus = 200;

    /**
     * @var CompanyTransformer
     */
    protected $companyTransformer;

    /**
     * CompanyApiController constructor.
     * @param CompanyTransformer $companyTransformer
     */
    public function __construct(CompanyTransformer $companyTransformer)
    {
        $this->companyTransformer = $companyTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->input('status');

        $paginateData = Company::with([
            'type', 'category', 'subcategory', 'country', 'district', 'city', 'language'
        ]);

        if ($status != 'all') {
            $paginateData->where('status', $status);
        }

        $paginateData = $paginateData->paginate(10)
            ->toArray();

        $companies = $paginateData['data'];

        unset($paginateData['data']);

        return response()->json([
            'success' => [
                'message' => 'Success',
                'companies' => $this->companyTransformer->transformCollection($companies),
                'pagination' => $paginateData
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type_id' => 'required|exists:company_types,id',
            'category_id' => 'required|exists:categories,id',
            'country_id' => 'required|exists:countries,id',
            'district_id' => 'required|exists:districts,id',
            'city_id' => 'required|exists:cities,id',
            'address' => 'required',
            'fax' => 'required',
            'language_id' => 'required|exists:languages,id'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $inputs = $request->all();
        $inputs['user_id'] = Auth::user()->id;

        $company = Company::create($inputs);

        return $this->show($company->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $company = Company::with([
            'type', 'category', 'subcategory', 'country', 'district', 'city', 'language'
        ])
            ->where('id', $id)
            ->first();

        if (is_null($company)) {
            return $this->respondNotFound('Company not found');
        }

        return response()->json([
            'success' => [
                'message' => 'Success',
                'company' => $this->companyTransformer->transform($company->toArray())
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type_id' => 'required|exists:company_types,id',
            'category_id' => 'required|exists:categories,id',
            'country_id' => 'required|exists:countries,id',
            'district_id' => 'required|exists:districts,id',
            'city_id' => 'required|exists:cities,id',
            'address' => 'required',
            'fax' => 'required',
            'language_id' => 'required|exists:languages,id'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $company = Company::find($id);

        if (is_null($company)) {
            return $this->respondNotFound('Company not found');
        }

        $inputs = $request->all();

        $company->update($inputs);

        return $this->show($company->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $company = Company::find($id);

        if (is_null($company)) {
            return $this->respondNotFound('Company not found');
        }

        try {
            $company->delete();
        } catch (\Exception $e) {
            Log::error('Company delete failed @CompanyApiController@destroy');
        }

        return response()->json([
            'success' => [
                'message' => 'Successfully deleted company',
                'company' => $company
            ]
        ], $this->successStatus);
    }

    /**
     * Update status by Admin
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|in:0,1,2,3'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $company = Company::find($id);

        if (is_null($company)) {
            return $this->respondNotFound('Company not found');
        }

        $company->update([
            'status' => $request->input('status'),
            'updated_by' => Auth::user()->id
        ]);

        return $this->show($id);
    }

    /**
     * Get companies summary
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCompaniesSummary()
    {
        $summary = Company::select(DB::raw('status, SUM(`id`) as status_count'))
            ->groupBy('status')
            ->get()
            ->toArray();

        $total = 0;

        foreach ($summary as $s) {
            $total += $s['status_count'];
        }

        $summary[] = [
            'status' => 'all',
            'status_count' => $total
        ];

        return response()->json([
            'success' => [
                'message' => 'Success',
                'summary' => $summary
            ]
        ], $this->successStatus);
    }
}
