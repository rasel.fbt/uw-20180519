<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthApiController extends ApiController
{
    /**
     * @var int
     */
    public $successStatus = 200;

    /**
     * Registration Api
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'phone' => 'required',
            'phone_confirmation' => 'required|same:phone',
            'email' => 'required|email',
            'email_confirmation' => 'required|same:email',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
            'has_accepted_policy' => 'required',
            'type' => 'required|in:user,admin'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated($validator->errors());
        }

        $hasUser = User::where('email', $request->input('email'))->first();

        if ($hasUser) {
            return $this->respondNotValidated('User with email already exists.');
        }

        $input = $request->except(['name']);
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);

        $token = $user->createToken('MyApp')->accessToken;

        return response()->json([
            'success' => [
                'message' => 'Successfully registered',
                'user' => [
                    'name' => $user->name
                ],
                'token' => $token
            ]
        ], $this->successStatus);
    }

    /**
     * Login Api
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        if (
        Auth::attempt([
            'email' => request('email'),
            'password' => request('password')
        ])
        ) {
            $user = Auth::user();
            $token = $user->createToken('MyApp')->accessToken;

            return response()->json([
                'success' => [
                    'message' => 'Successfully logged in',
                    'user' => $user,
                    'token' => $token
                ]
            ], $this->successStatus);
        }

        return $this->respondNotAuthorized('Unauthorized');
    }

    /**
     * Get authenticated user details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthUserDetails()
    {
        $user = Auth::user();

        return response()->json([
            'success' => [
                'message' => 'Success',
                'user' => $user
            ]
        ], $this->successStatus);
    }

    /**
     * Logout user api
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        if (Auth::check()) {
            Auth::user()->AauthAcessToken()->delete();

            return response()->json([
                'success' => [
                    'message' => 'You are successfully logged out'
                ]
            ], $this->successStatus);
        }

        return $this->respondNotAuthorized();
    }
}
