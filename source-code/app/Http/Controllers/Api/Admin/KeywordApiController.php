<?php

namespace App\Http\Controllers\Api\Admin;

use App\Acme\Transformers\KeywordTransformer;
use App\Models\Keyword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\ApiController;

class KeywordApiController extends ApiController
{
    public $successStatus = 200;

    /**
     * @var KeywordTransformer
     */
    protected $keywordTransformer;

    /**
     * KeywordApiController constructor.
     *
     * @param KeywordTransformer $keywordTransformer
     */
    public function __construct(KeywordTransformer $keywordTransformer)
    {
        $this->keywordTransformer = $keywordTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name.*' => 'required',
            'category_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $keywordIds = [];
        $keywordNames = $request->input('name');

        foreach ($keywordNames as $keywordName) {
            $keyword = Keyword::create([
                'name' => $keywordName,
                'category_id' => $request->input('category_id')
            ]);

            $keywordIds[] = $keyword->id;
        }

        $keywords = Keyword::whereIn('id', $keywordIds)
            ->get()
            ->toArray();

        return response()->json([
            'success' => [
                'message' => 'Success',
                'keywords' => $this->keywordTransformer->transformCollection($keywords),
            ]
        ], $this->successStatus);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $keyword = Keyword::find($id);

        if (is_null($keyword)) {
            return $this->respondNotFound('Keyword not found');
        }

        $keyword = Keyword::with(['category'])
            ->where('id', $keyword->id)
            ->first();

        return response()->json([
            'success' => [
                'message' => 'Success',
                'keyword' => $this->keywordTransformer->transform($keyword->toArray())
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $keyword = Keyword::find($id);

        if (is_null($keyword)) {
            return $this->respondNotFound('Keyword not found');
        }

        $inputs = $request->all();

        $keyword->update($inputs);

        return $this->show($keyword->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $keyword = Keyword::find($id);

        if (is_null($keyword)) {
            return $this->respondNotFound('Keyword not found');
        }

        try {
            $keyword->delete();
        } catch (\Exception $e) {
            Log::error('Keyword delete failed @KeywordApiController@destroy');
        }

        return response()->json([
            'success' => [
                'message' => 'Successfully deleted category',
                'keyword' => $keyword
            ]
        ], $this->successStatus);
    }
}
