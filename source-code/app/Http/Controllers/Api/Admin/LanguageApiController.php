<?php

namespace App\Http\Controllers\Api\Admin;

use App\Acme\Transformers\LanguageTransformer;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\ApiController;

class LanguageApiController extends ApiController
{
    public $successStatus = 200;
    /**
     * @var LanguageTransformer
     */
    protected $languageTransformer;

    /**
     * AuthApiController constructor.
     *
     * @param LanguageTransformer $languageTransformer
     */
    public function __construct(LanguageTransformer $languageTransformer)
    {
        $this->languageTransformer = $languageTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginateData = Language::paginate(10)
            ->toArray();

        $languages = $paginateData['data'];

        unset($paginateData['data']);

        return response()->json([
            'success' => [
                'message' => 'Success',
                'languages' => $this->languageTransformer->transformCollection($languages),
                'pagination' => $paginateData
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $inputs = $request->all();

        $language = Language::create($inputs);

        return $this->show($language->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $language = Language::find($id);

        if (is_null($language)) {
            return $this->respondNotFound('Language not found');
        }

        return response()->json([
            'success' => [
                'message' => 'Success',
                'language' => $this->languageTransformer->transform($language->toArray())
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $language = Language::find($id);

        if (is_null($language)) {
            return $this->respondNotFound('Language not found');
        }

        $inputs = $request->all();

        $language->update($inputs);

        return $this->show($language->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $language = Language::find($id);

        if (is_null($language)) {
            return $this->respondNotFound('Language not found');
        }

        try {
            $language->delete();
        } catch (\Exception $e) {
            Log::error('Language delete failed @LanguageApiController@destroy');
        }

        return response()->json([
            'success' => [
                'message' => 'Successfully deleted language',
                'language' => $language
            ]
        ], $this->successStatus);
    }
}
