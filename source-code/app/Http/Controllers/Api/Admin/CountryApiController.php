<?php

namespace App\Http\Controllers\Api\Admin;

use App\Acme\Transformers\CountryTransformer;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\ApiController;

class CountryApiController extends ApiController
{
    public $successStatus = 200;
    /**
     * @var CountryTransformer
     */
    protected $countryTransformer;

    /**
     * AuthApiController constructor.
     *
     * @param CountryTransformer $countryTransformer
     */
    public function __construct(CountryTransformer $countryTransformer)
    {
        $this->countryTransformer = $countryTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginateData = Country::with(['districts', 'districts.cities'])->paginate(10)
            ->toArray();

        $countries = $paginateData['data'];

        unset($paginateData['data']);

        return response()->json([
            'success' => [
                'message' => 'Success',
                'countries' => $this->countryTransformer->transformCollection($countries),
                'pagination' => $paginateData
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $inputs = $request->all();

        $country = Country::create($inputs);

        return $this->show($country->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $country = Country::with(['districts', 'districts.cities'])->find($id);

        if (is_null($country)) {
            return $this->respondNotFound('Country not found');
        }

        return response()->json([
            'success' => [
                'message' => 'Success',
                'country' => $this->countryTransformer->transform($country->toArray())
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $country = Country::find($id);

        if (is_null($country)) {
            return $this->respondNotFound('Country not found');
        }

        $inputs = $request->all();

        $country->update($inputs);

        return $this->show($country->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $country = Country::find($id);

        if (is_null($country)) {
            return $this->respondNotFound('Country not found');
        }

        try {
            $country->delete();
        } catch (\Exception $e) {
            Log::error('Country delete failed @CountryApiController@destroy');
        }

        return response()->json([
            'success' => [
                'message' => 'Successfully deleted country',
                'country' => $country
            ]
        ], $this->successStatus);
    }
}
