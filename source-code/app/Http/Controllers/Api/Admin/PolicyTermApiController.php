<?php

namespace App\Http\Controllers\Api\Admin;

use App\Acme\Transformers\PolicyTermTransformer;
use App\Models\PolicyTerm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\ApiController;

class PolicyTermApiController extends ApiController
{
    public $successStatus = 200;

    /**
     * @var PolicyTermTransformer
     */
    protected $policyTermTransformer;

    /**
     * AuthApiController constructor.
     *
     * @param PolicyTermTransformer $policyTermTransformer
     */
    public function __construct(PolicyTermTransformer $policyTermTransformer)
    {
        $this->policyTermTransformer = $policyTermTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginateData = PolicyTerm::paginate(10)
            ->toArray();

        $policyTerms = $paginateData['data'];

        unset($paginateData['data']);

        return response()->json([
            'success' => [
                'message' => 'Success',
                'policyTerms' => $this->policyTermTransformer->transformCollection($policyTerms),
                'pagination' => $paginateData
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $inputs = $request->all();

        $policyTerm = PolicyTerm::create($inputs);

        return $this->show($policyTerm->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $policyTerm = PolicyTerm::find($id);

        if (is_null($policyTerm)) {
            return $this->respondNotFound('PolicyTerm not found');
        }

        return response()->json([
            'success' => [
                'message' => 'Success',
                'policyTerm' => $this->policyTermTransformer->transform($policyTerm->toArray())
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $policyTerm = PolicyTerm::find($id);

        if (is_null($policyTerm)) {
            return $this->respondNotFound('Policy Term not found');
        }

        $inputs = $request->all();

        $policyTerm->update($inputs);

        return $this->show($policyTerm->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $policyTerm = PolicyTerm::find($id);

        if (is_null($policyTerm)) {
            return $this->respondNotFound('PolicyTerm not found');
        }

        try {
            $policyTerm->delete();
        } catch (\Exception $e) {
            Log::error('Policy Term delete failed @PolicyTermApiController@destroy');
        }

        return response()->json([
            'success' => [
                'message' => 'Successfully deleted policyTerm',
                'policyTerm' => $policyTerm
            ]
        ], $this->successStatus);
    }
}
