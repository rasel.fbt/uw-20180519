<?php

namespace App\Http\Controllers\Api\Admin;

use App\Acme\Transformers\CategoryTransformer;
use App\Acme\Transformers\KeywordTransformer;
use App\Http\Controllers\Api\ApiController;
use App\Http\Traits\FileTrait;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CategoryApiController extends ApiController
{
    use FileTrait;

    public $successStatus = 200;

    /**
     * @var CategoryTransformer
     */
    protected $categoryTransformer;

    /**
     * @var KeywordTransformer
     */
    protected $keywordTransformer;

    /**
     * AuthApiController constructor.
     *
     * @param CategoryTransformer $categoryTransformer
     * @param KeywordTransformer $keywordTransformer
     */
    public function __construct(CategoryTransformer $categoryTransformer, KeywordTransformer $keywordTransformer)
    {
        $this->categoryTransformer = $categoryTransformer;
        $this->keywordTransformer = $keywordTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginateData = Category::with(['subcategories', 'subcategories.keywords'])
            ->paginate(10)
            ->toArray();

        $categories = $paginateData['data'];

        unset($paginateData['data']);

        return response()->json([
            'success' => [
                'message' => 'Success',
                'categories' => $this->categoryTransformer->transformCollection($categories),
                'pagination' => $paginateData
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name.*' => 'required',
            'parent_id' => 'sometimes|required'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $categoryIds = [];
        $categoryNames = $request->input('name');

        $image = $request->input('image');
        $imageArr = [];

        if (!is_null($image)) {
            $imageArr = explode(',', $image);
        }

        foreach ($categoryNames as $categoryName) {
            $category = Category::create([
                'name' => $categoryName,
                'parent_id' => $request->input('parent_id'),
                'image' => $this->saveFiles($imageArr)
            ]);

            $categoryIds[] = $category->id;
        }

        $categories = Category::whereIn('id', $categoryIds)
            ->get()
            ->toArray();

        return response()->json([
            'success' => [
                'message' => 'Success',
                'categories' => $this->categoryTransformer->transformCollection($categories),
            ]
        ], $this->successStatus);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $category = Category::with(['subcategories', 'subcategories.keywords'])
            ->find($id);

        if (is_null($category)) {
            return $this->respondNotFound('Category not found');
        }

        return response()->json([
            'success' => [
                'message' => 'Success',
                'category' => $this->categoryTransformer->transform($category->toArray())
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'parent_id' => 'sometimes|required'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $category = Category::find($id);

        if (is_null($category)) {
            return $this->respondNotFound('Category not found');
        }

        $inputs = $request->all();

        $image = $request->input('image');
        $imageArr = [];

        if (!is_null($image)) {
            $imageArr = explode(',', $image);
        }

        $inputs['image'] = $this->saveFiles($imageArr);

        $category->update($inputs);

        return $this->show($category->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        if (is_null($category)) {
            return $this->respondNotFound('Category not found');
        }

        try {
            $category->delete();
        } catch (\Exception $e) {
            Log::error('Category delete failed @CategoryApiController@destroy');
        }

        return response()->json([
            'success' => [
                'message' => 'Successfully deleted category',
                'category' => $category
            ]
        ], $this->successStatus);
    }

    /**
     * Get only categories
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function categories()
    {
        $paginateData = Category::whereNull('parent_id')
            ->with('subcategories')
            ->paginate(10)
            ->toArray();

        $categories = $paginateData['data'];

        unset($paginateData['data']);

        return response()->json([
            'success' => [
                'message' => 'Success',
                'categories' => $this->categoryTransformer->transformCollection($categories),
                'pagination' => $paginateData
            ]
        ], $this->successStatus);
    }
}
