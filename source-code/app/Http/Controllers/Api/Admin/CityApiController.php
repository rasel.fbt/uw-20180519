<?php

namespace App\Http\Controllers\Api\Admin;

use App\Acme\Transformers\CityTransformer;
use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\ApiController;

class CityApiController extends ApiController
{
    public $successStatus = 200;
    /**
     * @var cityTransformer
     */
    protected $cityTransformer;

    /**
     * AuthApiController constructor.
     *
     * @param CityTransformer $cityTransformer
     */
    public function __construct(CityTransformer $cityTransformer)
    {
        $this->cityTransformer = $cityTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginateData = City::with('district')
            ->paginate(10)
            ->toArray();

        $cities = $paginateData['data'];

        unset($paginateData['data']);

        return response()->json([
            'success' => [
                'message' => 'Success',
                'cities' => $this->cityTransformer->transformCollection($cities),
                'pagination' => $paginateData
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name.*' => 'required',
            'district_id' => 'required|exists:districts,id'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $cityIds = [];
        $cityNames = $request->input('name');

        foreach ($cityNames as $cityName) {
            $city = City::create([
                'name' => $cityName,
                'district_id' => $request->input('district_id')
            ]);

            $cityIds[] = $city->id;
        }

        $cities = City::whereIn('id', $cityIds)
            ->get()
            ->toArray();

        return response()->json([
            'success' => [
                'message' => 'Success',
                'categories' => $this->cityTransformer->transformCollection($cities),
            ]
        ], $this->successStatus);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $city = City::with('district')
            ->where('id', $id)
            ->first();

        if (is_null($city)) {
            return $this->respondNotFound('City not found');
        }

        return response()->json([
            'success' => [
                'message' => 'Success',
                'city' => $this->cityTransformer->transform($city->toArray())
            ]
        ], $this->successStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'district_id' => 'required|exists:districts,id'
        ]);

        if ($validator->fails()) {
            return $this->respondNotValidated(
                $validator->errors()->first()
            );
        }

        $city = City::find($id);

        if (is_null($city)) {
            return $this->respondNotFound('City not found');
        }

        $inputs = $request->all();

        $city->update($inputs);

        return $this->show($city->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $city = City::find($id);

        if (is_null($city)) {
            return $this->respondNotFound('City not found');
        }

        try {
            $city->delete();
        } catch (\Exception $e) {
            Log::error('City delete failed @CityApiController@destroy');
        }

        return response()->json([
            'success' => [
                'message' => 'Successfully deleted city',
                'city' => $city
            ]
        ], $this->successStatus);
    }
}
