<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    return response()->json([
        'success' => [
            'message' => 'Welcome to API'
        ]
    ], 200);
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'v1',
    'namespace' => 'Api'
], function () {
    Route::get('/', function () {
        return 'API v1';
    });

    /* Unauthenticated Routes */
    Route::post('register', 'AuthApiController@register');
    Route::post('login', 'AuthApiController@login');

    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        /* Authentication Routes */
        Route::post('logout', 'AuthApiController@logout');
        Route::get('get-auth-user-details', 'AuthApiController@getAuthUserDetails');

        Route::group([
            'middleware' => 'auth:api', // Some user checking middleware
            'namespace' => 'User',
            'prefix' => 'user'
        ], function () {
            Route::resource('languages', 'LanguageApiController', [
                'only' => ['index', 'show']
            ]);
            Route::resource('countries', 'CountryApiController', [
                'only' => ['index', 'show']
            ]);
            Route::resource('cities', 'CityApiController', [
                'only' => ['index', 'show']
            ]);
            Route::resource('districts', 'DistrictApiController', [
                'only' => ['index', 'show']
            ]);
            Route::resource('company-types', 'CompanyTypeApiController', [
                'only' => ['index', 'show']
            ]);
            Route::resource('policy-terms', 'PolicyTermApiController', [
                'only' => ['index', 'show']
            ]);
            Route::get('categories/categories', ['uses' => 'CategoryApiController@categories'])->name('categories.categories');
            Route::resource('categories', 'CategoryApiController', [
                'only' => ['index', 'show']
            ]);
            Route::resource('keywords', 'KeywordApiController', [
                'only' => ['show']
            ]);
            Route::resource('companies', 'CompanyApiController', [
                'only' => ['store', 'show', 'update']
            ]);
//        Route::get('job-posts/summary', 'JobPostApiController@getJobsSummary');
//        Route::post('job-posts/{job_post}/status', 'JobPostApiController@updateStatus');
            Route::resource('job-posts', 'JobPostApiController');
        });

        Route::group([
            'middleware' => ['auth:api', 'auth.admin'], // Some admin checking middleware
            'namespace' => 'Admin',
            'prefix' => 'admin'
        ], function () {
            Route::resource('languages', 'LanguageApiController');
            Route::resource('countries', 'CountryApiController');
            Route::resource('cities', 'CityApiController');
            Route::resource('districts', 'DistrictApiController');
            Route::resource('company-types', 'CompanyTypeApiController');
            Route::resource('policy-terms', 'PolicyTermApiController');
            Route::get('categories/categories', ['uses' => 'CategoryApiController@categories'])->name('categories.categories');
            Route::resource('categories', 'CategoryApiController');
            Route::resource('keywords', 'KeywordApiController', [
                'only' => ['store', 'show', 'update', 'destroy']
            ]);
            Route::get('companies/summary', 'CompanyApiController@getCompaniesSummary');
            Route::post('companies/{company}/status', 'CompanyApiController@updateStatus');
            Route::resource('companies', 'CompanyApiController');
        });

        // Common Authenticated Api
        Route::resource('uploads', 'UploadApiController');
    });
});
